+++
title = "Install on Fedora"
distro = "Fedora"
summary = "Install instructions for KiCad on Fedora"
iconhtml = "<div class='fl-fedora'></div>"
weight = 20
+++

== Stable Release

{{< repology fedora_rawhide >}}

{{< repology fedora_35 >}}

{{< repology fedora_34 >}}

In general, current Fedora releases ship the stable versions of KiCad as they are
released.  However, Fedora policy doesn't allow major version changes in numbered
releases.  For example, Fedora 35 initially included KiCad 5, and so it is not
appropriate to suddenly force every Fedora 35 user to upgrade to KiCad 6.  But for
those users who _do_ want to upgrade to KiCad 6, we've got you covered: we recommend
installing the stable flatpak version of KiCad 6 as described in
link:{{% ref path="flatpak.adoc" %}}[KiCad Flatpak].

To install the most recent native KiCad package on Fedora, simply search for it
in the Software app or open a terminal and run the following command:

[source,bash]
dnf install kicad

In case the latest KiCad release is not yet available in the updates repository,
chances are you'll find it in the testing repository. Run the following command
to install a testing package:

[source,bash]
dnf --enablerepo=updates-testing install kicad

As of KiCad 5.0.0, the 3D models have been moved to a separate package and can
be installed with:

[source,bash]
dnf install kicad-packages3d

== Nightly Development Builds

The _nightly development builds_ are snapshots of the development codebase
(master branch) at a specific time. This codebase is under active development,
and while we try our best, may contain more bugs than usual. New features added
to KiCad can be tested in these builds.

WARNING: These builds may be unstable, and projects edited with these may not be
usable with the current stable release. **Use at your own risk!**

TIP: Nightly builds are provided in a separate installation directory. It is
possible to install nightly builds at the same time as a stable version.

Nightly development builds for Fedora are available via the
link:https://copr.fedorainfracloud.org/coprs/g/kicad/kicad/[Copr build service],
and can be installed with:

[source,bash]
dnf copr enable @kicad/kicad
dnf install kicad-nightly

The kicad-nightly package includes the latest revision of all libraries except
for the 3D models from the
link:https://gitlab.com/kicad/libraries/kicad-packages3D[kicad-packages3D]
repository. Because of their size of more than 5 GB on disk they have been moved
to a separate package and can be installed with:

[source,bash]
dnf install kicad-nightly-packages3d

If you don't have Copr enabled yet, install it first with:

[source,bash]
dnf install dnf-plugins-core
