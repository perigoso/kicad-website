+++
title = "Install using Flatpak"
distro = "Flatpak"
summary = "Install instructions for KiCad on Linux using Flatpak"
iconhtml = "<div class='fl-flathub'></div>"
weight = 10
+++
:dist: Flatpak

== Stable Release
Version: image:https://img.shields.io/flathub/v/org.kicad.KiCad?color=bright[]

To install KiCad using the Software app:

image:https://flathub.org/assets/badges/flathub-badge-en.png[width=240px, link='https://flathub.org/repo/appstream/org.kicad.KiCad.flatpakref']

Alternatively, you can use the Terminal to install using `flatpak` command:

[source,bash]
flatpak install --from https://flathub.org/repo/appstream/org.kicad.KiCad.flatpakref

=== Flathub stable source

You can find the build manifest to build from source link:https://github.com/flathub/org.kicad.KiCad[here].

== Beta Release
Version: 6.0.0-rc2

To install the current KiCad pre-release version using the Software app:

image:https://flathub.org/assets/badges/flathub-badge-en.png[width=240px, link='https://flathub.org/beta-repo/appstream/org.kicad.KiCad.flatpakref']

Alternatively you can use the Terminal to install using `flatpak` command:

[source,bash]
flatpak install --user https://flathub.org/beta-repo/appstream/org.kicad.KiCad.flatpakref

=== Flathub beta source

You can find the build manifest to build from source link:https://github.com/flathub/org.kicad.KiCad/tree/beta[here].

== Switching between stable and beta releases

If you have both the stable and the beta release installed, you can run either explicitly:

[source,bash]
flatpak run org.kicad.KiCad//stable

or

[source,bash]
flatpak run org.kicad.KiCad//beta

If you want to switch which version your desktop environment shows in menus, use `make-current`:

[source,bash]
flatpak make-current --user org.kicad.KiCad stable

or

[source,bash]
flatpak make-current --user org.kicad.KiCad beta

Afterwards, the made current version will start whenever you run

[source,bash]
flatpak run org.kicad.KiCad
